from __future__ import division #used for the difference metric
import pickle
from difflib import get_close_matches
#load database
db = pickle.load(open("database/db.txt","rb"))
from math import atan, pi


dbs = {} #sum of database items, a cache for uniqueness metric. NOTE: this is kept per instance
for artist in db:
    for word in db[artist]:
        if word == "":
            continue
        if word in dbs:
            dbs[word] += db[artist][word]
        else:
            dbs[word] = db[artist][word]

def unique(word,artist):
    global db,dbs
    if word == "":
        return 0
    #use the uniqueness metric
    try:
        return 2*db[artist][word.encode('utf-8')]-dbs[word.encode('utf-8')]
    except:
        return 0

def metric(word,artist):
    global db
    return unique(word,artist) * len(db) + modifiedPop(word,artist)

def topN(artist,n=10,score="uni"):
    global db
    if not (artist in db):
        matches = get_close_matches(artist,list(db),1)
        if matches == []:
            return ["Artist not in database."]
        else:
            return ["Artist not in database. Did you mean %s?" % (get_close_matches(artist,list(db),1)[0])]
    n = int(n)
    maxWords = []
    if score == "pop":
        #rank the words
        dbr = {word:db[artist][word] for word in db[artist]}
        #find the n top words, based on their scores
        maxWords = [t[0] for t in sorted(dbr.iteritems(), key=lambda (k, v): (-v, k))[:n]]
        #return the words with the numbers inserted before
        return [str(k+1)+". "+maxWords[k] for k in range(0,n)]
    if score == "uni":
        dbr = {word:unique(word,artist) for word in db[artist]}
        #find the n top words, based on their scores
        maxWords = [t[0] for t in sorted(dbr.iteritems(), key=lambda (k, v): (-v, k))[:n]]
        #return the words with the numbers inserted before
        return [str(k+1)+". "+maxWords[k] for k in range(0,n)]
    
def avgNumber(word):
    global db, dbs
    if not (word in dbs):
        return "Word not in database."
    try:
        return float(dbs[word])/len(db)
    except KeyError:
        return 0
    
def adjust(x):
    return (2/pi) * atan(20*x)
    
def diff(a1,a2):
    global db
    score = 0
    n = len(db[a1])+len(db[a2])-len([val for val in db[a1] if val in db[a2]])
    for item in db[a1]:
        inc = 0
        if item in db[a2]:
            inc = db[a1][item]/db[a2][item]
        if inc > 1:
            inc = 1/inc
        score += inc
    return adjust(score/n)**(6)*3

def sim(a1,n=10):
    global db
    dbr = {a2:diff(a1,a2) for a2 in db if a2 != a1}
    maxWords = [t[0] for t in sorted(dbr.iteritems(), key=lambda (k, v): (-v, k))[:n]]
    return [str(k+1)+". "+maxWords[k] for k in range(0,n)]
